package iasi.bootcamp.springbootweb.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class ProductReqDTO implements Serializable {
    @NotBlank
    private String name;
    @Min(0)
    private Double price;

    public ProductReqDTO() {
    }

    public ProductReqDTO(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ProductReqDTO that = (ProductReqDTO) o;

        return new EqualsBuilder().append(name, that.name).append(price, that.price).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(name).append(price).toHashCode();
    }
}
