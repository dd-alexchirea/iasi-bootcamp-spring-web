package iasi.bootcamp.springbootweb.service;

import iasi.bootcamp.springbootweb.controller.error.CustomException;
import iasi.bootcamp.springbootweb.controller.error.ErrorCodes;
import iasi.bootcamp.springbootweb.dto.ProductReqAuxDTO;
import iasi.bootcamp.springbootweb.dto.ProductReqDTO;
import iasi.bootcamp.springbootweb.dto.ProductResDTO;
import iasi.bootcamp.springbootweb.model.Product;
import iasi.bootcamp.springbootweb.repository.ProductsRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductsRepository productsRepository;
    private final ModelMapper modelMapper;

    public ProductService(ProductsRepository productsRepository, ModelMapper modelMapper) {
        this.productsRepository = productsRepository;
        this.modelMapper = modelMapper;
    }

    public ProductResDTO get(Long id) {
        Optional<Product> foundProduct = productsRepository.findById(id);

        if (foundProduct.isPresent()) {
            return toDTO(foundProduct.get());
        }
        throw new CustomException(ErrorCodes.ENTITY_NOT_FOUND);
    }

    public List<ProductResDTO> get() {
        return productsRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    public ProductResDTO create(ProductReqDTO productReqDTO) {
        Product savedProduct = productsRepository.save(toProduct(productReqDTO));

        Product product = new Product(null, "Nume", 12.0);

        productsRepository.save(product);

        return toDTO(savedProduct);
    }

    private ProductResDTO toDTO(Product product) {
        return modelMapper.map(product, ProductResDTO.class);
    }

    private Product toProduct(ProductReqDTO productReqDTO) {
        return modelMapper.map(productReqDTO, Product.class);
    }

    public Product update(Long id, ProductReqDTO productReqDTO) {

        Optional<Product> dbProduct = productsRepository.findById(id);


        if (!dbProduct.isPresent()) throw new CustomException(ErrorCodes.ENTITY_NOT_FOUND);

        dbProduct.get().setName(productReqDTO.getName());
        dbProduct.get().setPrice(productReqDTO.getPrice());

        return productsRepository.save(dbProduct.get());

    }

    public void deleteById(Long id) {
        productsRepository.deleteById(id);
    }

    public Product getProductsByName(String name){
        return productsRepository.findByName(name);
    }

    public List<ProductResDTO> getAllInRange(Double min, Double max) {
        if (min == null) min = Double.MIN_VALUE;
        if (max == null) max = Double.MAX_VALUE;
        return productsRepository.findAllByPriceBetween(min, max)
                .stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    public Product  updatePatch(Long id, ProductReqAuxDTO productReqAuxDTO){
        Optional<Product> product = productsRepository.findById(id);

        if(!product.isPresent()) throw new CustomException(ErrorCodes.ENTITY_NOT_FOUND);

        if(!productReqAuxDTO.getName().trim().isEmpty())
            product.get().setName(productReqAuxDTO.getName());

        if(productReqAuxDTO.getPrice() != null && productReqAuxDTO.getPrice() >= 0)
            product.get().setPrice(productReqAuxDTO.getPrice());

        return productsRepository.save(product.get());
    }
}
