package iasi.bootcamp.springbootweb.controller;

import iasi.bootcamp.springbootweb.dto.ProductReqAuxDTO;
import iasi.bootcamp.springbootweb.dto.ProductReqDTO;
import iasi.bootcamp.springbootweb.dto.ProductResDTO;
import iasi.bootcamp.springbootweb.model.Product;
import iasi.bootcamp.springbootweb.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductsController {

    private final ProductService productService;

    public ProductsController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<List<ProductResDTO>> get() {
        return ResponseEntity.ok(productService.get());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductResDTO> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(productService.get(id));
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<Product> getByName(@PathVariable("name") String name) {
        return ResponseEntity.ok(productService.getProductsByName(name));
    }

    @GetMapping("/filter")
    public ResponseEntity<List<ProductResDTO>> filterByPrice(@RequestParam(value = "min", required = false) Double min,
                                                             @RequestParam(value = "max", required = false) Double max) {
        return ResponseEntity.ok(productService.getAllInRange(min, max));
    }

    @PostMapping
    public ResponseEntity<ProductResDTO> create(@RequestBody @Valid ProductReqDTO productReqDTO) {
        return ResponseEntity.ok(productService.create(productReqDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> update(@PathVariable("id") Long id,
                                          @RequestBody @Valid ProductReqDTO productReqDTO) {
        return ResponseEntity.ok(productService.update(id, productReqDTO));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Product> updatePatch(@PathVariable("id") Long id,
                                               @RequestBody ProductReqAuxDTO productReqAuxDTO) {
        return ResponseEntity.ok(productService.updatePatch(id, productReqAuxDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Product> delete(@PathVariable("id") Long id) {
        productService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
