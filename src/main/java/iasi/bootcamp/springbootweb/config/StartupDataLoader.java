package iasi.bootcamp.springbootweb.config;

import iasi.bootcamp.springbootweb.dto.ProductReqDTO;
import iasi.bootcamp.springbootweb.service.ProductService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class StartupDataLoader implements ApplicationRunner {

    private final ProductService productService;

    public StartupDataLoader(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        productService.create(new ProductReqDTO("Product 1", 10.23));
        productService.create(new ProductReqDTO("Product 2", 25.99));
        productService.create(new ProductReqDTO("Product 3", 67.00));
    }
}
