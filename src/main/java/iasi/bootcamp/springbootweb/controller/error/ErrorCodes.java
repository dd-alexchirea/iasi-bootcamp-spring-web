package iasi.bootcamp.springbootweb.controller.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public enum ErrorCodes {
    ENTITY_NOT_FOUND("ENTITY_NOT_FOUND", "The entity requested was not found!", HttpStatus.NOT_FOUND);

    private String code;
    private String messsage;
    private HttpStatus httpStatus;

    ErrorCodes(String code, String messsage, HttpStatus httpStatus) {
        this.code = code;
        this.messsage = messsage;
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return code;
    }

    public String getMesssage() {
        return messsage;
    }

    public ResponseEntity toResponseEntity() {
        Map<String, String> result = new HashMap<>();
        result.put("code", code);
        result.put("messsage", messsage);
        return new ResponseEntity(result, httpStatus);
    }
}
