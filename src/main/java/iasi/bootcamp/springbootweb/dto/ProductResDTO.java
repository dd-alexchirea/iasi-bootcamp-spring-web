package iasi.bootcamp.springbootweb.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class ProductResDTO extends ProductReqDTO {
    private Long id;

    public ProductResDTO() {
    }

    public ProductResDTO(String name, Double price, Long id) {
        super(name, price);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ProductResDTO that = (ProductResDTO) o;

        return new EqualsBuilder().appendSuper(super.equals(o)).append(id, that.id).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).appendSuper(super.hashCode()).append(id).toHashCode();
    }
}
