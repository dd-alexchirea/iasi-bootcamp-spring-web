package iasi.bootcamp.springbootweb.repository;

import iasi.bootcamp.springbootweb.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductsRepository extends JpaRepository<Product, Long> {
    public Product findByName(String name);

    List<Product> findAllByPriceBetween(Double min, Double max);
}
