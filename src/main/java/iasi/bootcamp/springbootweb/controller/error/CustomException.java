package iasi.bootcamp.springbootweb.controller.error;

import org.springframework.http.ResponseEntity;

public class CustomException extends RuntimeException {
    private ErrorCodes errorCode;

    public CustomException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }

    public ResponseEntity toResponse() {
        return errorCode.toResponseEntity();
    }
}
